import React from 'react';
import { StyleSheet, Text, View , Image,TouchableOpacity} from 'react-native';


export default function App() {
  return (
      <View style = {styles.container}>
            <View style = {{ width : '100%',height: 48, marginTop: 50 , marginLeft : 29,flexDirection: "row"}}>
              <Text style = {styles.about}>About Me</Text>
            </View>
            <View style = {{width : '100%', height: 190 , justifyContent:'center', alignItems : 'center', marginTop : 40}}>
              <Image source = {require('./assets/Untitled-1.jpg')} style = {{width : 136 , height : 136 , borderRadius : 100}}/>
              <Text style = {{fontSize: 18, fontWeight : '500', lineHeight : 25 , color : 'white', marginTop : 30}}>Jabal Nugraha</Text>
            </View>
            <View style = {{ width : '100%',height: 24, marginTop: 45 , marginLeft : 26, flexDirection: "row"}}>
              <Text style = {{fontSize : 18 , fontWeight : '600' , lineHeight :24 , color : 'white'}}>Social Media</Text>
            </View>
            <View style = {{ width : '100%', height : 96, marginTop: 16}}>
              <View style = {{width : '100%' , height : 24, flexDirection : 'row', marginBottom : 14, marginLeft : 66}}>
                <Image source = {require('./assets/ig.png')} style = {{tintColor : 'white' ,width :23,height:23}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@heyjabs_</Text>
                </TouchableOpacity>
              </View>
              <View style = {{width : '100%' , height : 24, flexDirection : 'row', marginBottom : 14,marginLeft : 66}}>
                <Image source = {require('./assets/fb.png')} style = {{tintColor : 'white' ,width :23,height:23}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@jabsunk</Text>
                </TouchableOpacity>
              </View>
              <View style = {{width :'100%' , height : 24, flexDirection : 'row', marginBottom : 14,marginLeft : 66}}>
                <Image source = {require('./assets/tw.png')} style = {{tintColor : 'white' ,width :23,height:23}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@balljabal</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style = {{ width : '100%',height: 24, marginTop: 48 , marginLeft : 26,flexDirection: "row"}}>
              <Text style = {{fontSize : 18 , fontWeight : '600' , lineHeight :24 , color : 'white'}}>My portofolio</Text>
            </View>
            <View style = {{ width : '100%', height : 96, marginTop: 16}}>
              <View style = {{width : '100%' , height : 24, flexDirection : 'row', marginBottom : 14,marginLeft : 66}}>
                <Image source = {require('./assets/gt.png')} style = {{tintColor : 'white' ,width :23,height:24}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@jabsunknown</Text>
                </TouchableOpacity>
              </View>
              <View style = {{width : '100%' , height : 24, flexDirection : 'row', marginBottom : 14,marginLeft : 66}}>
                <Image source = {require('./assets/dr.png')} style = {{tintColor : 'white' ,width :23,height:24}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@EIENdsg</Text>
                </TouchableOpacity>
              </View>
              <View style = {{width : '100%' , height : 24, flexDirection : 'row', marginBottom : 14,marginLeft : 66}}>
                <Image source = {require('./assets/ig.png')} style = {{tintColor : 'white' ,width :23,height:24}}/>
                <TouchableOpacity>
                  <Text style = {styles.touch}>@EIENdsg</Text>
                </TouchableOpacity>
              </View>
            </View>
      </View>

    );
  }

const styles = StyleSheet.create({
  container: {
   flex: 1,
  }, about : {
    fontSize : 20,
    lineHeight : 24,
    color: 'white',
    fontWeight: 'bold',
  }, touch: {
    fontSize : 15,
    lineHeight : 24,
    fontWeight : '600',
    fontStyle : 'normal',
    marginLeft: 16,
    color : '#8CA4E1'}
});
