import React from 'react';
import { StyleSheet, Text, View , Image,TouchableOpacity, ScrollView, FlatList} from 'react-native';
import skillData from './skillData.json'
import SkillItem from './skill'


export default function App() {
    return (
        <View style = {styles.container}>
           <View style = {styles.welcome}>
              <Text style = {{fontSize : 18, fontWeight:'100', color : 'white'}}>Welcome</Text>
              <Text style = {{fontSize : 18, fontWeight: 'bold', color : 'white'}}>Jabal Nugraha</Text>
            </View>
            <View style = {styles.yourskill}>
                <View style = {{alignItems:'center', marginTop : 17}}>
                <Text style = {{color:'black', fontSize: 18, fontWeight: 'bold' }}>
                    Yourskill
                </Text>
            </View>
            <View>
                <View style = {{Width :'100%', height: 24, marginLeft : 21, marginTop : 17, marginBottom : 17}}>
                <Text style = {{color:'black', fontSize : 16, fontWeight:'bold'}}>
                    Browse by category
                </Text>
                </View>            
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  style = {styles.category}>
                    <TouchableOpacity style = {styles.box}>
                        <Text style = {{color:'white', fontWeight: '900', fontSize: 13}}>Programming</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.box}>
                        <Text style = {{color:'white', fontWeight: '900', fontSize: 13}}>Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.box}>
                        <Text style = {{color:'white', fontWeight: '900', fontSize: 13}}>Technology</Text>
                    </TouchableOpacity>
                 </ScrollView>
                <View style = {{Width :'100%', height: 24, marginLeft : 21, marginBottom : 17, marginTop: 17}}>
                <Text style = {{color:'black', fontSize : 16, fontWeight:'bold'}}>
                    List of your skill
                </Text>
                </View>
                
                    {/* <View style = {styles.box2}>
                        <Text style = {{fontSize: 14, fontWeight : 'bold', marginTop : 5 , marginLeft :10}}>Programming</Text>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>JavaScript 100%</Text>
                        <Progress.Bar progress={1} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Python 66%</Text>
                        <Progress.Bar progress={0.66} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                            <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Objective - c 33%</Text>
                        <Progress.Bar progress={0.33} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                    </View>
                    <View style = {styles.box2}>
                    <Text style = {{fontSize: 14, fontWeight : 'bold', marginTop : 5 , marginLeft :10}}>Framework</Text>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>ReactNative 100%</Text>
                        <Progress.Bar progress={1} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Vue.js 66%</Text>
                        <Progress.Bar progress={0.66} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                            <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Angular 33%</Text>
                        <Progress.Bar progress={0.33} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        
                    </View>
                    <View style = {styles.box2}>
                    <Text style = {{fontSize: 14, fontWeight : 'bold', marginTop : 5 , marginLeft :10}}>Technology</Text>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Visual Studio Code 100%</Text>
                        <Progress.Bar progress={1} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Cmder 66%</Text>
                        <Progress.Bar progress={0.66} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                            <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        <View>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>Git 33%</Text>
                        <Progress.Bar progress={0.33} width={209} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center'}}><Text style = {{fontSize: 10}}>Advance</Text></View>
                            </View>
                        </View>
                        
                    </View> */}
                     <FlatList data={skillData.items}
                     renderItem={(skill)=><SkillItem skill={skill.item} />}
                     keyExtractor={(item)=>item.id.toString()}
                     horizontal/>
                 <TouchableOpacity style = {{width : '100%', height :24, alignItems : 'center', marginTop : 25}}>
                     <Text style = {styles.touch}>About Dev</Text>
                 </TouchableOpacity>
             </View>

            </View>
         
        </View>
  
      );
    }
  
  const styles = StyleSheet.create({
    container: {
     flex: 1,
    },
    welcome: {
        width : '100%',
        height: 49,
        marginTop: 50 ,
        marginLeft : 60,
        marginBottom: 25,
        flexDirection: "column"
    }, yourskill : {
        width: '100%',
        height: '100%' ,
        backgroundColor:'white',
        borderRadius:30,
    }, category: {
        width : "100%",
        height : 112,
        flexDirection : 'row'
    },    box:{
        width:112,
        height:112,
        backgroundColor: 'black',
        alignItems:'center',
        justifyContent:'center',
        borderRadius: 10 ,
        marginHorizontal: 12,
        shadowColor: "#000",
        shadowOffset: {
	    width: 0,
	    height: 24,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },  touch: {
        marginBottom: 14,
        color : '#8CA4E1'}
  });
  