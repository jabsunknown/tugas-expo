import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { LoginScreen } from './LoginScreen';
import { SkillScreen } from './SkillScreen';
import { AboutScreen} from './AboutScreen';
import { AddScreen } from './AddScreen';
import { ProjectScreen} from './ProjectScreen';


const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode='none' >
   <RootStack.Screen name="SignIn" component={DrawerScreen} />
  </RootStack.Navigator>
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="LoginScreen" component={LoginScreen} />
    <Drawer.Screen name="Profile" component={AboutScreen} />
    <Drawer.Screen name="Tabscreen" component={TabsScreen} />
  </Drawer.Navigator>
);

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    <Tabs.Navigator 
    tabBarOptions= {{
      labelStyle: {
        fontSize: 14,
        marginBottom: 5
      }
    }}>
      <Tabs.Screen name="SkillScreen" component={SkillScreen}/>
      <Tabs.Screen name="ProjectScreen" component={ProjectScreen} />
      <Tabs.Screen name="AddScreen" component={AddScreen} />
    </Tabs.Navigator>
  );




export default () => {
    return (
        <NavigationContainer>
          <RootStackScreen />
        </NavigationContainer>
    );
  };




