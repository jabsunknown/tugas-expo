import React from 'react';
import { StyleSheet, Text, View , Image, TextInput, TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import {StatusBar} from 'expo-status-bar';


export const LoginScreen = () => {
  return (<LinearGradient
    colors = {['#39435C','#000000','#39435C']}
    start={{ x: 0, y: 0 }}
    end={{ x: 1, y: 1 }}
    style = {styles.container}>
    <StatusBar translucent = {false} backgroundColor={"white"}/>
          <View style = {styles.logocontainer}>
          <Image source={require('./assets/logo.png')} style={styles.logo} />
          <Text style = {styles.textlogo}>SanberApp</Text>
            <TextInput 
            placeholder = "Email/username"
            placeholderTextColor ="rgba(17, 17, 17, 0.48)"
            style = {styles.input} />
             <TextInput 
            placeholder = "Password"
            placeholderTextColor ="rgba(17, 17, 17, 0.48)"
            style = {styles.input} />
             <View>
             <TouchableOpacity>
               <Text style = {styles.touch}>Forget Password?</Text>
               </TouchableOpacity> 
             </View> 
            <TouchableOpacity style = {styles.LoginButton}>
              <Text style = {styles.btnTXT}>Login</Text>
            </TouchableOpacity>
            <View style = {styles.Dont}>
              <Text style = {styles.text}>Don't have an account?</Text>
             <TouchableOpacity>
               <Text style = {styles.touch}>Register here</Text>
               </TouchableOpacity> 
            </View>
          </View>
      </LinearGradient>

    );
  }

const styles = StyleSheet.create({
  container: {
   flex: 1,

  },
  logocontainer: {
    alignItems: 'center',
    flexGrow : 1,
    justifyContent : 'center'
  },
  logo: {
    width: 110.47,
    height: 136.94,
  },
  textlogo: {
      justifyContent: 'center' ,
      color: 'white',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 30,
      lineHeight: 30,
      textAlign: 'center',
      alignItems: 'center',
      elevation: 3,
      width: 187,
      height: 48,
      marginTop:20,
  },
  input: {
    height: 56,
    marginBottom : 14,
    width: 316,
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 4,
  
}, touch: {
  marginBottom: 14,
  color : '#8CA4E1'
}, LoginButton: {
  height: 56,
  marginBottom : 18,
  width: 316,
  backgroundColor: '#39435C',
  padding: 16,
  borderRadius: 10,
  justifyContent: 'center',
  alignItems: 'center',

}, btnTXT: {
  justifyContent: 'center' ,
  color: 'white',
  fontStyle: 'normal',
  fontWeight: '300',
  fontSize: 17,
  lineHeight: 30,
  textAlign: 'center',
  alignItems: 'center',
  elevation: 3,
}, Dont : {
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
}, text: {
  color : 'white'
}
});
