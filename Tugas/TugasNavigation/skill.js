import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Progress from 'react-native-progress';


export default class skill extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <ScrollView horizontal showsHorizontalScrollIndicator={true} style = {styles.category2}>

                <View style = {styles.box2}> 
                <Icon name={ skill.iconName } size={75}/>
                        <View style = {{alignItems: 'center'}}>
                            <Text style = {{fontSize: 14, fontWeight : 'bold', marginTop : 5 , marginLeft :10}}>{skill.categoryName}</Text>
                            <Text style = {{fontSize : 12 , fontWeight : "300", marginTop : 5, marginLeft :10}}>{skill.skillName}</Text>
                            <Text style = {{fontSize : 14 , fontWeight : "bold", marginTop : 5, marginLeft :10}}>{skill.percentageProgress}</Text>
                            <Progress.Bar progress={skill.percentageProgressBar} width={200} height={16} borderRadius={10} color={'#54D06F'}/>
                        <View style={{flexDirection: 'row', height : 26,}}>
                                <View style = {{width : 69, alignItems : 'center', marginTop: 10}}><Text style = {{fontSize: 11}}>Basic</Text></View>
                                <View style = {{width : 69, alignItems : 'center', marginTop: 10}}><Text style = {{fontSize: 11}}>Intermediate</Text></View>
                                <View style = {{width : 69, alignItems : 'center', marginTop: 10}}><Text style = {{fontSize: 11}}>Advance</Text></View>
                            </View>
                </View>
                        
                    </View>
                    </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
         box2:{
             alignItems:'center',
        justifyContent : 'center',
        width:270,
        height:250,
        backgroundColor: 'white',
        borderRadius: 10 ,
        marginHorizontal: 12,
        shadowColor: "#000",
        shadowOffset: {
	    width: 3,
	    height: 24,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, category2: {
        height : 270,
    }

});

                